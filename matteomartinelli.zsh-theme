# Matteo Martinelli's theme
#
# This a theme for oh-my-zsh. Features a colored prompt with:
# * username: [jobs] [git] workdir % 
# * hostname color is based on hostname characters. When using as root, the 
# prompt shows only the hostname in red color.
# * [jobs], if applicable, counts the number of suspended jobs tty
# * [git], if applicable, represents the status of your git repo (more on that
# later)
# * '%' prompt will be green if last command return value is 0, yellow otherwise.
# 
# git prompt is inspired by official git contrib prompt: 
# https://github.com/git/git/tree/master/contrib/completion/git-prompt.sh
# and it adds:
# * the current branch
# * '%' if there are untracked files
# * '$' if there are stashed changes
# * '*' if there are modified files
# * '+' if there are added files
# * '<' if local repo is behind remote repo
# * '>' if local repo is ahead remote repo
# * '=' if local repo is equal to remote repo (in sync)
# * '<>' if local repo is diverged

local green="%{$fg[green]%}"
local red="%{$fg[red]%}"
local cyan="%{$fg[cyan]%}"
local yellow="%{$fg[yellow]%}"
local blue="%{$fg[blue]%}"
local magenta="%{$fg[magenta]%}"
local white="%{$fg[white]%}"
local reset="%{$reset_color%}"

local -a color_array
color_array=($green $red $cyan $yellow $blue $magenta $white)

local username_normal_color=$red
local hostname_normal_color=$red
local username_root_color=$red
local hostname_root_color=$red

# calculating hostname color with hostname characters
#for i in `hostname`; local hostname_normal_color=$color_array[$[((#i))%7+1]]
#local -a hostname_color
hostname_color=%(!.$hostname_root_color.$hostname_normal_color)

local current_dir_color=$white
local username_command="%n"
local hostname_command="%m"
local current_dir="%~"

local username_output="%(!..$username_normal_color$username_command$reset)"
local hostname_output="@$hostname_color$hostname_command$reset"
local current_dir_output="$current_dir_color$current_dir$reset"
local jobs_bg="${red}fg: %j$reset"
#local last_command_output="%(?.%(!.$red.$green).$yellow)"
local last_command_output=""

ZSH_THEME_GIT_PROMPT_PREFIX=""
ZSH_THEME_GIT_PROMPT_SUFFIX=""
ZSH_THEME_GIT_PROMPT_DIRTY=""
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_UNTRACKED=" $red%%"
ZSH_THEME_GIT_PROMPT_MODIFIED=" $red*"
ZSH_THEME_GIT_PROMPT_ADDED=" $red+"
#ZSH_THEME_GIT_PROMPT_STASHED=" $red$"
#ZSH_THEME_GIT_PROMPT_STASHED=""
ZSH_THEME_GIT_PROMPT_EQUAL_REMOTE="synced"
ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE="ahead"
ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE="behind"
ZSH_THEME_GIT_PROMPT_DIVERGED_REMOTE="diverged"

PROMPT='[$username_output:$current_dir_output%1(j. [$jobs_bg].)]'
GIT_PROMPT='$(out=$(git_prompt_info)$(git_prompt_status);if [[ -n $out ]]; then printf %s " $green($out$green)$reset";fi)'
#PROMPT+="$GIT_PROMPT"
PROMPT+=" $last_command_output$ $reset"
RPROMPT="$GIT_PROMPT"
